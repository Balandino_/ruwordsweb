package org.russian.sql;


public abstract class Statements {
	
	public static final String extractBare = "SELECT bare, type FROM variants WHERE variant = ? ";
	
	public static final String extractNoun = "SELECT * FROM nouns WHERE bare = ? ";
	
	public static final String extractAdjective = "SELECT * FROM adjectives WHERE bare = ? ";
	
	public static final String extractOther = "SELECT * FROM others WHERE bare = ? ";
	
	public static final String extractVerbAndPartners = "SELECT * FROM verbs WHERE bare = ? "
			+ "UNION ALL "
			+ "SELECT * FROM verbs WHERE bare IN ("
			+ "    WITH RECURSIVE split(value, str) AS ("
			+ "        SELECT null, (SELECT partner FROM verbs WHERE bare = ? ) || ';' "
			+ "        UNION ALL "
			+ "        SELECT substr(str, 0, instr(str, ';')), substr(str, instr(str, ';') +1) "
			+ "    FROM split WHERE str!='') SELECT value FROM split WHERE value is not NULL "
			+ ")";
	
	
	
//	PreparedStatement preppedBareSearch = conn.prepareStatement("SELECT bare, type FROM variants WHERE variant = ? ");
//	PreparedStatement preppedNoun = conn.prepareStatement("SELECT * FROM nouns WHERE bare = ? ");
//	PreparedStatement preppedAdjective = conn.prepareStatement("SELECT * FROM adjectives WHERE bare = ? ");
//	PreparedStatement preppedOther = conn.prepareStatement("SELECT * FROM others WHERE bare = ? ");
//	PreparedStatement preppedVerbAndPartners = conn.prepareStatement("SELECT * FROM verbs WHERE bare = ? "
//			+ "UNION ALL "
//			+ "SELECT * FROM verbs WHERE bare IN ("
//			+ "    WITH RECURSIVE split(value, str) AS ("
//			+ "        SELECT null, (SELECT partner FROM verbs WHERE bare = ? ) || ';' "
//			+ "        UNION ALL "
//			+ "        SELECT substr(str, 0, instr(str, ';')), substr(str, instr(str, ';') +1) "
//			+ "    FROM split WHERE str!='') SELECT value FROM split WHERE value is not NULL "
//			+ ")");
	
}
