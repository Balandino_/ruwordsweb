package org.russian.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.russian.sql.Statements;
import org.sqlite.SQLiteConfig;

//1024*1024 * 2 = 2MB
@MultipartConfig(location = "",  fileSizeThreshold = 1024*1024*2,  maxFileSize = 1024*1024*2, maxRequestSize = 1024*1024*2)
@WebServlet("/ReadDB")
public class ReadDatabaseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static boolean driverLoaded = false;
	
	static {
		try {
			//Load JDBC Driver
			Class.forName("org.sqlite.JDBC");
			driverLoaded = true;
		} catch (ClassNotFoundException error) {
			System.err.println(error.getLocalizedMessage());
		}
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
		
			//Prepare response
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			
			Part importedFile = request.getPart("ruwords");
		    InputStream fileContent = importedFile.getInputStream();
		    
		    final boolean inMemory = false;
			final String database = "/home/mkg/Git/RuWordsWeb/RuWordsWeb/Database/Russian_Words_Lite_Modified.sqlite3";
		    //final String database = "/database/Russian_Words_Lite_Modified.sqlite3";
			final String url = (inMemory ? "jdbc:sqlite::memory:" : ("jdbc:sqlite:" + database));
			
			SQLiteConfig config = new SQLiteConfig();
			config.setReadOnly((inMemory ? false : true));
			config.setSharedCache(true);
			
			
			if(driverLoaded) {
				
				try (Connection conn = DriverManager.getConnection(url, config.toProperties())) {
					
					try (Statement statement = conn.createStatement()) {
						if (inMemory) {
							statement.execute("restore from " + "'" + database +  "'");
						}
						//System.out.println("Connection to " + (inMemory ? "in-memory " : "") +  "database has been established!");
						
						//Prepare Statements
						PreparedStatement preppedBareSearch = conn.prepareStatement(Statements.extractBare);
						PreparedStatement preppedNoun = conn.prepareStatement(Statements.extractNoun);
						PreparedStatement preppedAdjective = conn.prepareStatement(Statements.extractAdjective);
						PreparedStatement preppedOther = conn.prepareStatement(Statements.extractOther);
						PreparedStatement preppedVerbAndPartners = conn.prepareStatement(Statements.extractVerbAndPartners);
						
						//Prepare some statistics
						long wordCount = 0;
						long matchedCount = 0;
						
						//For results to be stored
						StringBuilder allDefinitions = new StringBuilder("{ \"words\": [");
						
						Pattern pattern = Pattern.compile("([А-Яа-яЁё-]+[А-Яа-яЁё]+)|([А-Яа-яЁё]+)", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
						HashSet<String> previouslyMatched = new HashSet<String>();
						HashSet<String> unknownWords = new HashSet<String>();
						
						try(BufferedReader reader = new BufferedReader(new InputStreamReader(fileContent))){
							String line = "";
							while ((line = reader.readLine()) != null) {
								Matcher matcher = pattern.matcher(line);
								while(matcher.find()) {
									String searchWord = matcher.group().toLowerCase();
									wordCount++;
		
									if (previouslyMatched.contains(searchWord) || unknownWords.contains(searchWord)) {// Skip duplicates
										if (previouslyMatched.contains(searchWord)) {
											matchedCount++;
										}
										continue;
									}
									
									preppedBareSearch.setString(1, searchWord.toLowerCase());
		
									// Get bare word & type matches
									StringBuilder searchResult = new StringBuilder();
									ResultSet results = preppedBareSearch.executeQuery();
									while (results.next()) {
										searchResult.append(results.getString(1) + "," + results.getString(2));
										searchResult.append("|");
									}
		
									// Confirm matches are present
									if (searchResult.length() == 0) {
										unknownWords.add(searchWord);
										continue;
									}
		
									// Process all matches
									StringBuilder extract = new StringBuilder("{" + "\"" + searchWord + "\": [");
									String[] wordsAndTypes = searchResult.toString().substring(0, searchResult.length() - 1).split("\\|");
									
									boolean alreadyLoopedThrough = false; //Used to detect if multiple types (e.g noun & verb) are used in the loop
									for (String match : wordsAndTypes) {
										String[] wordAndType = match.split(",");
										switch (wordAndType[1]) {
										case "verb":
											ReadDatabaseServlet.editIfPreviousMatch(alreadyLoopedThrough, extract);//Edits string to allow new array for next word type
											extract.append(ReadDatabaseServlet.searchForWordEntry(preppedVerbAndPartners, 19, 1, 2, wordAndType[0]));
											alreadyLoopedThrough = true;
											break;
		
										case "noun":
											ReadDatabaseServlet.editIfPreviousMatch(alreadyLoopedThrough, extract);//Edits string to allow new array for next word type
											extract.append(ReadDatabaseServlet.searchForWordEntry(preppedNoun, 16, 2, 1, wordAndType[0]));
											alreadyLoopedThrough = true;
											break;
		
										case "adjective":
											ReadDatabaseServlet.editIfPreviousMatch(alreadyLoopedThrough, extract);//Edits string to allow new array for next word type
											extract.append(ReadDatabaseServlet.searchForWordEntry(preppedAdjective, 17, 4, 1,	wordAndType[0]));
											alreadyLoopedThrough = true;
											break;
		
										case "other":
											ReadDatabaseServlet.editIfPreviousMatch(alreadyLoopedThrough, extract);//Edits string to allow new array for next word type
											extract.append(ReadDatabaseServlet.searchForWordEntry(preppedOther, 5, 1, 1, wordAndType[0]));
											alreadyLoopedThrough = true;
											break;
		
										default:
											System.out.println("Unknown type: " + wordAndType[1]);
											unknownWords.add(searchWord);
											continue;
										}
									}
									matchedCount++;
									allDefinitions.append(extract.toString());
									previouslyMatched.add(searchWord);
								}
							}
						}
						
						
						// Formats results in the event of no words matched
						if(matchedCount == 0) {
							allDefinitions.append("]");
						}
						
						
						String statistics = ",\"statistics\": [{\"matched\" : \"" + matchedCount 
												+ "\"}, {\"unmatched\" : \"" + (wordCount - matchedCount) 
												+ "\"}, {\"total\" : \"" + wordCount + "\"} ]";
						
						
						
						//Get the container which will be returned in the response
						String container = ",\"container\": \"" + request.getParameter("container") + "\"";					
						
						allDefinitions.replace(allDefinitions.length() -1, allDefinitions.length(), "]" + statistics + container + "}");//Corrects ending to allow proper JSON formatting
						
						// Debugging
						//Files.deleteIfExists(Paths.get("/tmp/allDefinitions.txt"));
						//Files.writeString(Paths.get("/tmp/allDefinitions.txt"), allDefinitions);
						
						// Send response
						out.write(allDefinitions.toString());
					}
				
				} catch (SQLException sqlEx) {
					sqlEx.printStackTrace();
					out.write(sqlEx.getLocalizedMessage());
				} 
			}
		} catch (IllegalStateException ex) {
			// File size too large
			if(ex.getLocalizedMessage().startsWith("org.apache.tomcat.util.http.fileupload.impl.SizeLimitExceededException")) {
				response.setContentType("text/html");
				response.setCharacterEncoding("UTF-8");
				PrintWriter out = response.getWriter();
				out.write("SizeError");
			} 
		}
	}


	private static String searchForWordEntry(PreparedStatement prepped, int columnCount, int rowCount, int argCount, String bareWord) throws SQLException {
		for(int i = 1; i < argCount + 1; i++) {
			prepped.setString(i, bareWord.toLowerCase());
		}

		ResultSet results = prepped.executeQuery();
		
		String wordType = "";
		LinkedHashMap<String, String[]> merger = new LinkedHashMap<String, String[]>();
		int rowsWritten = 0;
		
		boolean matchFound = false;
		int rowIterationCount = 1;
		while(results.next()) {
			matchFound = true;	//Match Confirmed
			wordType = results.getString(4); // Get type
			String[] wordData = new String[2]; //[0] = conjugations/declensions, [1] = translation
			
			String header = results.getString(1) + "," + results.getString(2); //Header will be used to check for duplicate data
			String translation = results.getString(columnCount); //Translations, which will be combined if multiple ones exist
						
			ResultSetMetaData metaData = results.getMetaData();
			
			//TESTING - For debugging to cut down on output
			//columnCount = 2;
			
			StringBuilder dataLine = new StringBuilder();
			for(int i = 1; i < columnCount; i++) {
				String end = (i == columnCount ? "" : ",");
				dataLine.append("\"W" + rowIterationCount + "_" + metaData.getColumnName(i) + "\":\"" + results.getString(i) + "\"" + end); //Build up the word data for this entry
			}
			
			wordData[0] = dataLine.toString(); 
			wordData[1] = translation;
			
			if(!merger.containsKey(header)) { //Add to HashMap in order, preserving orderings for verbs
				merger.put(header, wordData);
				rowsWritten = 0;// Reset as this could be a new word
				rowsWritten++; //Increment as 1 row has now been written
			} else {
				String[] mergedData = merger.get(header);//Get current detail
				
				if(rowsWritten < rowCount) {//If more rows are to be written, otherwise it should be duplicate data
					mergedData[0] = mergedData[0] + wordData[0]; //Merge
					rowsWritten++; 
				}
				
				if(!(mergedData[1].indexOf(wordData[1]) > -1)) {
					mergedData[1] = mergedData[1] + ", " + wordData[1]; //Merge any translations
					merger.put(header, mergedData); //Update
				}
			}
			rowIterationCount++;
		}
		
		
		//Builds a string from captured data for processing in JSON format
		StringBuilder encodedResults = new StringBuilder("{\"" + wordType + "s\": [");
		for(String wordHeader : merger.keySet()) {
			String[] wordData = merger.get(wordHeader);
			encodedResults.append("{");
			String combo = wordData[0] + "\"" + "Translation" + "\":" + "\"" + wordData [1] + "\"";
			encodedResults.append(combo + "},");
		}
		
		encodedResults.replace(encodedResults.length() -1, encodedResults.length(), "]}]},");//Correct the ending when finished
		
		
		return (matchFound == true ? encodedResults.toString() : "null");
	}

private static void editIfPreviousMatch(boolean previousMatch, StringBuilder extract) {
	if(previousMatch) {
		extract.replace(extract.length() -3, extract.length(), ",");
	}
}



}
