// Globals start  //////////////////////////////////////////////////////////

// As the tabs can operate independently, these will prevent multiple
// requests being used at once and confusing the feedback mechanisms  
//var ajaxLock = false;
//var lockMessage = "";
var ajaxResponseTarget;
var ajaxInfoDiv;

// Used to detect when subtitles should be being returned
var ajaxPurpose = "";

// Regex to extract russian words
var regexp = /[а-яА-ЯёЁ]+-{0,1}[а-яА-ЯёЁ]{0,}/g;

// Used for dictionary tab
var regexpNonGlobal = /[а-яА-ЯёЁ]+-{0,1}[а-яА-ЯёЁ]{0,}/;

// Used on dictionary tab
//var regexpNonGlobal = /[а-яА-ЯёЁ]+-{0,1}[а-яА-ЯёЁ]{0,1}/;

// If the YouTube tab is used and subtitles are found, then 
// these will hold the subtitles in memory when parsed
// from their xml format.
var youtubeRussianSubsXML = "";
var youtubeEnglishSubsXML = "";

// confirms whether the ajax calls for YouTueb subtitles were successful
var russianSubsReceived = false;
var englishSubsReceived = false;

// Holds perfective verbs from subtitles for highlight option
var youtubePerfectiveVerbs = [];

// Holds words that could be perfectives but have multiple definitions so can't be certain
var youtubePerfectiveVerbsMultipleDefinitions = [];

// Holds the code of the currently chosen video
var youtubeVideoCode = "";

// Holds the number referencing the method that calls subtitles repeatedly  
var interval = [];

// Reference to the youtube player when loaded
var player;

// Confirms whether the YouTube player has been loaded for the first time
var playerInitialised = false;

// Confirms if the YouTube tab is showing video, infoDiv, filter e.t.c
var youtubeVideoDisplayed = false;

// Holds the offset for the YouTube subtitles.
var youtubeDelay = 0.0;

// Confirms if these options are currently being used
var youtubeEnlarged = false;
var youtubeAspectRatio = false;
var youtubeInfoDivEnlarged = false

// Holds the offset for the YouTube subtitles.
var youtubeTranslations = [];

// Holds any subtitles uploaded for local video playback
var localRuSubsFileContent;
var localEnSubsFileContent;

// Confirms if local subtitles have been read
var localRuSubsProcessed = false;
var localEnSubsProcessed = false;

// Holds the parsed local subtitles
var parsedLocalRuSubs = [];
var parsedLocalEnSubs = [];

// Confirms if subtitles were submitted for processing
// Used for infoText responses
var localRuSubsSubmitted = false;
var localEnSubsSubmitted = false;

// Holds the number referencing the method that calls local video subtitles repeatedly  
var localVideoInterval = [];

// Holds perfective verbs from subtitles for highlight option, local video
var localPerfectiveVerbs = [];
// Holds words that could be perfectives but have multiple definitions so can't be certain
var localPerfectiveVerbsMultipleDefinitions = [];

// Holds the offset for the local subtitles.
var localDelay = 0.0;

// Holds the offset for the Local subtitles.
var localTranslations = [];

// Confirms if these opotions are currently being used
var localEnlarged = false;
var localAspectRatio = false;
var localInfoDivEnlarged = false;

// Confirms if the dictionary tab has been used for layout purposes on that tab
var dictionaryUsed = false;

// Confirms if key bindings have been turned on
var keyBindingsOn = false;

// Determines if the filter was reached via accessibility keys
// boolean and number combine to block out filter rest on shift + s keypress
var accessibilityJumpToFilter = false;
var accessibilityJumpCount = 0;


// Globals end  //////////////////////////////////////////////////////////


// Make asynchronous request
function sendAjax(methodtype, url, data) {
	var xmlHTTP = new XMLHttpRequest();

	xmlHTTP.onreadystatechange = function() {
		if (xmlHTTP.readyState == 4 && xmlHTTP.status == 200) {//Successful response
			var response = xmlHTTP.responseText;


			// Data submitted was above server's limit'
			if (response == "SizeError") {
				setInfoDivText(ajaxInfoDiv, "Error, amount of data submitted was too large");
				return;
			}

			//console.log("Status: " + xmlHTTP.status);
			//console.log("State: " + xmlHTTP.readyState);

			// YouTube subtitle search
			if (ajaxPurpose == "ensubs") {

				if (response.length == 0) {
					englishSubsReceived = false;

					// Set text
					setInfoDivText("youtubeInfoDiv", "English subtitles could not be obtained, locating Russian subtitles");
				} else {
					youtubeEnglishSubsXML = new DOMParser().parseFromString(response, "text/xml");
					englishSubsReceived = true;

					// Set text
					setInfoDivText("youtubeInfoDiv", "English subtitles loaded, locating Russian subtitles");
				}

				// Request Russian subtitles
				sendAjax("POST", "https://video.google.com/timedtext?lang=ru&v=" + youtubeVideoCode, null);
				ajaxPurpose = "rusubs"

			} else if (ajaxPurpose == "rusubs") {

				if (response.length == 0) {
					russianSubsReceived = false;

					// Set text
					setInfoDivText("youtubeInfoDiv", "Russian subtitles could not be obtained.");
				} else {
					youtubeRussianSubsXML = new DOMParser().parseFromString(response, "text/xml");
					russianSubsReceived = true;

					// Set text
					setInfoDivText("youtubeInfoDiv", "Russian subtitles loaded, acquiring translations.<br />This may take several seconds.");
				}


				//var regexp = /[а-яА-ЯёЁ]+-{0,1}[а-яА-ЯёЁ]{0,1}/g;
				var ruwords = "";
				var result;
				var duplicates = {};

				// Filter out any duplicate words
				while ((result = regexp.exec(response)) !== null) {
					result = String(result).toLowerCase();
					if (duplicates.hasOwnProperty(result)) {
						continue;
					} else {
						duplicates[result] = true;
						ruwords = ruwords + (result + " ");
					}
				}

				// Set target for response
				ajaxResponseTarget = document.getElementById("youtubeWordListContainer");


				if (ruwords == "") {
					var infoText = "";

					infoText = infoText + ((englishSubsReceived == true) ? "English subtitles loaded successfully, " : "Unable to locate English subtitles<br />");
					infoText = infoText + ((russianSubsReceived == true) ? "Russian subtitles loaded successfully <br />" : "Unable to locate Russian subtitles <br />");

					infoText = infoText + "Translation statistics: 0% Matched | 0 Words Submitted";
					setInfoDivText(ajaxInfoDiv, infoText);
				} else {

					// Send words to server for processing
					var parameters = new FormData();
					parameters.append("ruwords", ruwords);
					parameters.append("container", "youtubeTab");
					sendAjax("POST", "../ReadDB", parameters);
					ajaxPurpose = "";
				}

			} else {// JSON response from Tomcat

				var parsedData = JSON.parse(response);
				var parsedWords = parsedData["words"];
				var unorderedWordList = document.createElement("ul");
				var returnedContainer = parsedData["container"];

				if (returnedContainer == "youtubeTab") {
					unorderedWordList.setAttribute("id", "youtubeUL");
				} else if (returnedContainer == "localTab") {
					unorderedWordList.setAttribute("id", "localUL");
				} else {
					unorderedWordList.setAttribute("id", "wordsUL");
				}


				//unorderedWordList.setAttribute("id", "wordsUL");
				ajaxResponseTarget.appendChild(unorderedWordList);
				createListFromJson(parsedWords, unorderedWordList);

				//Get perfective verbs and translations
				if (returnedContainer == "youtubeTab") {
					getPerfectives(parsedWords, youtubePerfectiveVerbs, youtubePerfectiveVerbsMultipleDefinitions);
					getTranslations(parsedWords, "youtube");

					var displayTranslationsButton = document.getElementById("youtubeDisplayTranslations");
					// If another video has been chosen and this option is still in use, load translations
					if (displayTranslationsButton.value == "Hide List Translations") {
						displayTranslationsButton.value = "Display List Translations"; // Reset button before pressing
						displayTranslations('youtubeUL', 'youtube')
					}



				} else if (returnedContainer == "localTab") {
					getPerfectives(parsedWords, localPerfectiveVerbs, localPerfectiveVerbsMultipleDefinitions);
					getTranslations(parsedWords, "local");

					var displayTranslationsButton = document.getElementById("localVideoDisplayTranslations");
					// If another video has been chosen and this option is still in use, load translations
					if (displayTranslationsButton.value == "Hide List Translations") {
						displayTranslationsButton.value = "Display List Translations"; // Reset button before pressing
						displayTranslations("localUL", "local")
					}


				}


				//Handle Statistics
				var parsedStats = parsedData["statistics"];
				var matched = parsedStats[0]["matched"];
				var unmatched = parsedStats[1]["unmatched"];
				var total = parsedStats[2]["total"];

				var allMatched = (matched == total && total > 0);
				var noneMatched = (unmatched == total);


				var statistics;
				var statisticsEnding = (total > 1) ? " Words Submitted" : " Word Submitted";

				if (allMatched) {//100% matched
					statistics = "100% Matched | " + total + statisticsEnding
				} else if (noneMatched) {//0% matched
					statistics = "0% Matched | " + total + statisticsEnding
				} else {//"some matched, some not matched"

					matched = (matched == 0 ? 0 : (matched / total) * 100).toFixed(2);
					unmatched = (unmatched == 0 ? 0 : (unmatched / total) * 100).toFixed(2);
					statistics = matched + "% Matched,  " + unmatched + "% Unmatched, " + total + statisticsEnding
				}

				var infoText = "";

				if (ajaxInfoDiv == "youtubeInfoDiv") {
					infoText = infoText + ((englishSubsReceived == true) ? "English subtitles loaded successfully, " : "Unable to locate English subtitles<br />");
					infoText = infoText + ((russianSubsReceived == true) ? "Russian subtitles loaded successfully <br />" : "Unable to locate Russian subtitles <br />");
					infoText = infoText + "Translation statistics: " + statistics;

				} else if (ajaxInfoDiv == "localVideoInfoDiv") {

					infoText = infoText + (localRuSubsProcessed == true ? "Russian subtitles loaded<br />" : "No Russian subtitles have been processed<br />");
					infoText = infoText + (localEnSubsProcessed == true ? "English subtitles loaded<br />" : "No English subtitles have been processed<br />");
					infoText = infoText + "Translation statistics: " + statistics;
				} else {
					infoText = "Translation statistics: " + statistics;
				}

				setInfoDivText(ajaxInfoDiv, infoText);

				var curTab = getActiveNavTab();
				if (curTab == "youtubeTab") {
					setLayout(youtubeEnlarged, youtubeAspectRatio, true, curTab);
				} else if (curTab == "localTab") {
					setLayout(localEnlarged, localAspectRatio, true, curTab);
				} else {
					setLayout("resize");
				}

			} // End of Tomcat response


		} else {

			/*
			console.log("Status: " + xmlHTTP.status);
			console.log("State: " + xmlHTTP.readyState);
			console.log("============");
			*/

			var stateValues = ["Request not initialised", "Server connection established",
				"Request received", "Processing Request"];


			var infoText = "";

			switch (xmlHTTP.readyState) {

				case 0:
					infoText = stateValues[xmlHTTP.readyState] + ", Status: " + xmlHTTP.readyState;
					break;

				case 1:
					infoText = stateValues[xmlHTTP.readyState] + ", Status: " + xmlHTTP.readyState;
					break;

				case 2:
					if (xmlHTTP.status == 404) {
						infoText = "404 Error, data could not be found.<br />Perhaps the submitted data are incorrect or there is an error on the remote server.";
						setInfoDivText(ajaxInfoDiv, infoText);
						ajaxPurpose = "";
					}

					if (xmlHTTP.status == 500) {
						infoText = "500 Error, there may be an issue with the input or with the remote server.";
						setInfoDivText(ajaxInfoDiv, infoText);
						ajaxPurpose = "";
					}

					infoText = stateValues[xmlHTTP.readyState] + ", Status: " + xmlHTTP.readyState;
					break;

				case 3:
					infoText = stateValues[xmlHTTP.readyState] + ", Status: " + xmlHTTP.readyState;
					break;

			}
		}
	}

	if (methodtype == 'POST') {//Sending data

		xmlHTTP.open(methodtype, url, true);
		//xmlHTTP.setRequestHeader("Content-type", "multipart/form-data;boundary=" + boundary);
		xmlHTTP.send(data);
	}
}

function setInfoDivText(infoDivElement, infoText) {
	var infoDiv = document.getElementById(infoDivElement);
	var infoDivText = infoDiv.children[0];
	infoDivText.innerHTML = infoText;

}

function changeDelay(target, increase) {
	if (target == "youtube") {
		youtubeDelay = (increase == true) ? youtubeDelay + 0.5 : youtubeDelay - 0.5;
		document.getElementById("youtubeIncreaseDelay").value = "Raise Subtitle Delay (" + youtubeDelay.toString() + "s)";
		document.getElementById("youtubeDecreaseDelay").value = "Lower Subtitle Delay (" + youtubeDelay.toString() + "s)";
	} else {
		localDelay = (increase == true) ? localDelay + 0.5 : localDelay - 0.5;
		document.getElementById("localVideoIncreaseDelay").value = "Raise Subtitle Delay (" + localDelay.toString() + "s)";
		document.getElementById("localVideoDecreaseDelay").value = "Lower Subtitle Delay (" + localDelay.toString() + "s)";
	}
}


function changeInfoDivTextSize(element, enlarge) {
	var fontSize = parseFloat(window.getComputedStyle(element, null).getPropertyValue('font-size'));
	element.style.fontSize = (enlarge == true) ? (fontSize + 3) + "px" : (fontSize - 3) + "px"
	setLayout("resize");
}

function enlargeVideo(target) {

	var enlargeButton;
	var infoDiv;
	var infoDiv;
	var enlargeButton;
	var targetTab;

	if (target == "youtube") {
		enlargeButton = document.getElementById("youtubeEnlargeVideo");
		infoDiv = document.getElementById("youtubeInfoDiv");
		aspectButtonText = document.getElementById("youtube219").value;
		targetTab = "youtubeTab";
	} else {
		enlargeButton = document.getElementById("localVideoEnlargeVideo");
		infoDiv = document.getElementById("localVideoInfoDiv");
		aspectButtonText = document.getElementById("localVideo219").value;
		targetTab = "localTab";
	}


	if (enlargeButton.value == "Enlarge text area") {
		infoDiv.style.height = "20vh";
		enlargeButton.value = "Shrink text area";
		(target == "youtube") ? youtubeInfoDivEnlarged = true : localInfoDivEnlarged = true;
		setLayout("resize");
	} else if (enlargeButton.value == "Shrink text area") {
		infoDiv.style.height = "12vh";
		enlargeButton.value = "Enlarge text area";
		(target == "youtube") ? youtubeInfoDivEnlarged = false : localInfoDivEnlarged = false;
		setLayout("resize");
	} else {
		var enlarge = false;

		if (enlargeButton.value == "Shrink Video") {
			enlargeButton.value = "Enlarge Video";
		} else {
			enlargeButton.value = "Shrink Video";
			enlarge = true;
		}


		if (target == "youtube") {
			youtubeEnlarged = enlarge;
			aspect = youtubeAspectRatio;
		} else {
			localEnlarged = enlarge;
			aspect = localAspectRatio;
		}
		setLayout(enlarge, aspect, true, targetTab);
	}
}

function setTo21By9(target) {

	var aspectButton;
	var targetTab;
	var enlarge;
	var aspect;


	if (target == "youtube") {
		aspectButton = document.getElementById("youtube219");
		enlargeButtonText = document.getElementById("youtubeEnlargeVideo").value;
		targetTab = "youtubeTab";
		enlarge = youtubeEnlarged;
		youtubeAspectRatio = (youtubeAspectRatio == true) ? false : true;
		aspect = youtubeAspectRatio;


	} else {
		aspectButton = document.getElementById("localVideo219");
		enlargeButtonText = document.getElementById("localVideoEnlargeVideo").value;
		targetTab = "localTab";
		enlarge = localEnlarged;
		localAspectRatio = (localAspectRatio == true) ? false : true;
		aspect = localAspectRatio;
	}

	aspectButton.value = (aspect == true) ? "16:9" : "21:9";

	setLayout(enlarge, aspect, true, targetTab);

}


function turnOnColouredPerfectives(target) {

	var perfectiveButton;

	if (target == "youtube") {
		perfectiveButton = document.getElementById("youtubeColourPerfectives");
	} else {
		perfectiveButton = document.getElementById("localVideoColourPerfectives");
	}


	if (perfectiveButton.value == "Decolour Perfective Verbs") {
		perfectiveButton.value = "Colour Perfective Verbs";
	} else {
		perfectiveButton.value = "Decolour Perfective Verbs";
	}
}


function getTranslations(words, tab) {

	var container = (tab == 'youtube') ? youtubeTranslations : localTranslations;

	for (searchWord in words) {//Get array of words
		earlyExit:
		for (bareWord in words[searchWord]) {//All data associated with a word

			var typesMatched = [];
			var translation = "";

			//console.log("Bare: " + bareWord);
			for (type in words[searchWord][bareWord]) {//Get array of word types (e.g nouns, verbs)
				for (typeEntry in words[searchWord][bareWord][type]) {//Get each type
					for (entry in words[searchWord][bareWord][type][typeEntry]) {//Each word definition within the type
						var definition = words[searchWord][bareWord][type][typeEntry][entry];
						//var jsonKeys = Object.keys(definition);
						// console.log("Type: " + typeEntry);

						if (typesMatched.length == 0) {// Add first entry
							typesMatched.push(typeEntry);
						} else if (!typesMatched.includes(typeEntry)) {//If multiple definitions, exit
							translation = "Multiple translations available";
							break earlyExit;
						} else {// Add next definition
							typesMatched.push(typeEntry);
						}

						if (translation == "") {// First translation is the one we want
							translation = definition["Translation"];
						}
					}
				}
			}
		}

		// Translations	
		container.push(translation);
	}
}


function displayTranslations(wordList, tab) {

	var button;
	var translations;
	var addTranslations = true

	// Tab check	
	if (tab == "youtube") {
		button = document.getElementById("youtubeDisplayTranslations");
		translations = youtubeTranslations;
	} else {
		button = document.getElementById("localVideoDisplayTranslations");
		translations = localTranslations;
	}


	// check if adding translations or hiding them
	if (button.value == "Hide List Translations") {
		addTranslations = false;
		button.value = "Display List Translations";
	} else {
		button.value = "Hide List Translations";
	}

	// Loop over the word list
	var list = document.getElementById(wordList);
	if (list) {
		var liElements = document.getElementById(wordList).children;
		var count = 0;
		for (element of liElements) {
			var headerWordText = element.children[0].children[0].textContent;

			// Add or remove translation
			if (addTranslations == true) {
				element.children[0].children[0].textContent = "";

				// Bold helps the word stand out against the translation
				element.children[0].children[0].innerHTML = "<strong>" + headerWordText + "</strong> - " + translations[count];
				count = count + 1;
			} else {

				var inner = element.children[0].children[0].innerHTML;
				var bareWord = inner.substring(8, inner.indexOf("</"));
				element.children[0].children[0].innerHTML = "";
				element.children[0].children[0].textContent = bareWord;

			}
		}
	}
}

function getPerfectives(words, container, multipleDefinitionsContainer) {

	for (searchWord in words) {//Get array of words
		for (bareWord in words[searchWord]) {//All data associated with a word

			//var potentialPerfective = bareWord;
			var checkOnlyVerbsInDefinition = true;
			var perfectiveMatchFound = false;

			breakThisToGoToNextWord:
			for (type in words[searchWord][bareWord]) {//Get array of word types (e.g nouns, verbs)
				for (typeEntry in words[searchWord][bareWord][type]) {//Get each type
					for (entry in words[searchWord][bareWord][type][typeEntry]) {//Each word definition within the type
						var definition = words[searchWord][bareWord][type][typeEntry][entry];
						//var jsonKeys = Object.keys(definition);

						switch (typeEntry) {
							case "nouns":
								checkOnlyVerbsInDefinition = false;
								if (perfectiveMatchFound == true) { multipleDefinitionsContainer.push(bareWord); };

								break breakThisToGoToNextWord;

							case "adjectives":
								checkOnlyVerbsInDefinition = false;
								if (perfectiveMatchFound == true) { multipleDefinitionsContainer.push(bareWord); };
								break breakThisToGoToNextWord;

							case "verbs":
								if (definition["W1_aspect"] == "perfective") {
									if (perfectiveMatchFound == true) { multipleDefinitionsContainer.push(bareWord); };
									perfectiveMatchFound = true;
								}
								break;

							case "others":
								checkOnlyVerbsInDefinition = false;
								if (perfectiveMatchFound == true) { multipleDefinitionsContainer.push(bareWord); };
								break breakThisToGoToNextWord;

							default:
								checkOnlyVerbsInDefinition = false;
								if (perfectiveMatchFound == true) { multipleDefinitionsContainer.push(bareWord); };
								break breakThisToGoToNextWord;
						}
					}
				}
			}

			// Add perfective verb to global list			
			if (checkOnlyVerbsInDefinition == true && perfectiveMatchFound == true) {
				container.push(bareWord);
			}
		}
	}
}

function parseLocalSubs(fileContent, language) {


	var timestampRegex = /([0-9]{2}:[0-9]{2}:[0-9]{2}[,.]{1}[0-9]{0,}) --> ([0-9]{2}:[0-9]{2}:[0-9]{2}[,.]{1}[0-9]{0,})/
	var processedSubs = (language == "ru" ? parsedLocalRuSubs : parsedLocalEnSubs);

	for (var i = 0; i < fileContent.length; i++) {

		var times = timestampRegex.exec(fileContent[i]);

		if (times) {

			var splitTime = times[1].substring(0, 8).split(":");
			var seconds = parseFloat((+splitTime[0]) * 60 * 60 + (+splitTime[1]) * 60 + (+splitTime[2]));
			var miliseconds = parseFloat("0." + times[1].substring(9));
			var startTime = seconds + miliseconds
			processedSubs.push(startTime);

			splitTime = times[2].substring(0, 8).split(":");
			seconds = parseFloat((+splitTime[0]) * 60 * 60 + (+splitTime[1]) * 60 + (+splitTime[2]));
			miliseconds = parseFloat("0." + times[2].substring(9));
			var endTime = seconds + miliseconds
			processedSubs.push(endTime);

			var words = "";
			i++;



			while (fileContent[i].length > 0) {
				words += fileContent[i] + "<br />";
				i++;

				if (i >= fileContent.length) {
					break;
				}
			}

			words = words.substring(0, words.length - 6);
			processedSubs.push(words);
		}

	}

}



function getAndDisplayLocalSubtitles() {
	setInfoDivText("localVideoInfoDiv", getLocalSubtitles());
}

function getLocalSubtitles() {

	var matchedSubtitles = "";
	var playtime = document.getElementById("localVideoContainer").currentTime;

	// Handle delay changes by the user
	if (localDelay > 0) { playtime = playtime - localDelay };
	if (localDelay < 0) { playtime = playtime - localDelay };

	if (parsedLocalRuSubs.length > 0) {
		var ruIndex = 0;

		while (ruIndex < parsedLocalRuSubs.length) {
			if (playtime > parsedLocalRuSubs[ruIndex] && playtime < parsedLocalRuSubs[ruIndex + 1]) {
				matchedSubtitles = matchedSubtitles + parsedLocalRuSubs[ruIndex + 2];
				break;
			}
			ruIndex = ruIndex + 3
		}

		// If the button has been pushed, then we want to colour the relevant verbs
		if (document.getElementById("localVideoColourPerfectives").value == "Decolour Perfective Verbs") {
			var words = matchedSubtitles.split(" ");
			var subtitlesToReturn = "";

			for (word of words) {
				var match = regexpNonGlobal.exec(word);
				if (match != null) {
					var matchedWord = match[0]
					if (localPerfectiveVerbs.includes(matchedWord.toLowerCase())) {

						var colouredText = word.replace(matchedWord, '<span style="color: #179757">' + matchedWord + '</span>');
						subtitlesToReturn = subtitlesToReturn + colouredText + " ";

					} else if (localPerfectiveVerbsMultipleDefinitions.includes(matchedWord.toLowerCase())) {

						var colouredText = word.replace(matchedWord, '<span style="color: #f0544f">' + matchedWord + '</span>');
						subtitlesToReturn = subtitlesToReturn + colouredText + " ";
					} else {
						subtitlesToReturn = subtitlesToReturn + word + " ";
					}

				} else {
					subtitlesToReturn = subtitlesToReturn + word + " ";
				}
			}
			matchedSubtitles = subtitlesToReturn;
		}


		matchedSubtitles = matchedSubtitles + "<br /><br />";

		if (parsedLocalEnSubs.length > 0) {
			var enIndex = 0;
			while (enIndex < parsedLocalEnSubs.length) {
				if (playtime > parsedLocalEnSubs[enIndex] && playtime < parsedLocalEnSubs[enIndex + 1]) {
					matchedSubtitles = matchedSubtitles + parsedLocalEnSubs[enIndex + 2];
					break;
				}
				enIndex = enIndex + 3
			}
		}

	}
	return matchedSubtitles;
}

function processDictionary(textAreaId) {

	// Clears previous searches
	if (dictionaryUsed == true) {
		if (document.body.contains(document.getElementById("wordsUL"))) {
			document.getElementById("wordsUL").parentNode.removeChild(document.getElementById("wordsUL"));
		}
	}

	// Get words
	//var words = document.getElementById(textAreaId).value.split(/\r?\n/);
	var words = document.getElementById(textAreaId).value;


	words = words.replaceAll("[\n\r]", "");
	words = words.split(/[ ,]+/);

	var ruwords = [];
	for (word of words) {
		if (regexpNonGlobal.test(word)) {
			if (!ruwords.includes(word)) {
				ruwords.push(word.toLowerCase());
			}
		}
	}



	if (ruwords.length > 0) {
		// Reveal filter fields
		var elementsToReveal = ["dictionaryFilterFieldLabel", "dictionaryFilterField"];
		for (element of elementsToReveal) {
			var el = document.getElementById(element);
			el.style.display = "block";
			el.style.opacity = 1;
		}

		// Send words to server for processing
		ajaxResponseTarget = document.getElementById("dictionaryWordListContainer");
		ajaxInfoDiv = "dictionaryInfoDiv";
		var parameters = new FormData();
		parameters.append("ruwords", ruwords);
		parameters.append("container", "dictionaryTab");
		sendAjax("POST", "../ReadDB", parameters);

		// Confirm for layout changes
		dictionaryUsed = true;

		setInfoDivText("dictionaryInfoDiv", "Processing submitted words, please wait. . .");

	} else {
		setInfoDivText("dictionaryInfoDiv", "No russian words detected!");
	}
}


// read subtitle files
function readFile(file, language) {
	var reader = new FileReader();
	reader.readAsText(file, "UTF-8");
	reader.onload = function(evt) {



		if (language == "ru") {
			localRuSubsFileContent = evt.target.result.split(/\r?\n/);
			parseLocalSubs(localRuSubsFileContent, "ru");
			localRuSubsProcessed = true;

			// Get words for sending to server			
			//var regexp = /[а-яА-ЯёЁ]+-{0,1}[а-яА-ЯёЁ]{0,1}/g;
			var ruwords = [];

			while ((result = regexp.exec(localRuSubsFileContent)) !== null) {
				if (!ruwords.includes(String(result).toLowerCase())) {
					ruwords.push(String(result).toLowerCase());
				}
			}


			ajaxResponseTarget = document.getElementById("localVideoWordListContainer");
			ajaxInfoDiv = "localVideoInfoDiv";

			// Send words to server for processing
			var parameters = new FormData();
			parameters.append("ruwords", ruwords);
			parameters.append("container", "localTab");
			sendAjax("POST", "../ReadDB", parameters);


		} else {
			localEnSubsFileContent = evt.target.result.split(/\r?\n/);
			parseLocalSubs(localEnSubsFileContent, "en");
			localEnSubsProcessed = true;

			if (localRuSubsSubmitted != true) {
				setInfoDivText("localVideoInfoDiv", "English subtitles loaded<br />No Russian subtitles have been selected");
			}
		}
	}

	reader.onerror = function(/*evt*/) {

		if (language == "ru") {
			localRuSubsFileContent = "unread";
		} else {
			localEnSubsFileContent = "unread";
		}
	}
}


function processLocalVideo() {
	var uploadedVideo = document.getElementById("localVideoUpload").files[0];
	var uploadedRuSubs = document.getElementById("localVideoRuSubsUpload").files[0];
	var uploadedEnSubs = document.getElementById("localVideoEnSubsUpload").files[0];

	// Exit early if video not supplied
	if (uploadedVideo == undefined) {
		alert("A video from your device must be selected before proceeding");
		return;
	} else if (!uploadedVideo.name.endsWith("mp4") && !uploadedVideo.name.endsWith(".ogg") && !uploadedVideo.name.endsWith(".webm")) {
		alert("Only mp4, ogg or webm  formats are currently supported");
		return;
	}

	if (uploadedEnSubs != undefined) {
		localEnSubsSubmitted = true;
		readFile(uploadedEnSubs, "en");
	}

	if (uploadedRuSubs != undefined) {
		readFile(uploadedRuSubs, "ru");
		localRuSubsSubmitted = true;
	}

	// Determines correct output for infoDiv for user feedback
	if (localEnSubsSubmitted == true && localRuSubsSubmitted == false) {
		setInfoDivText("localVideoInfoDiv", "Processing English subtitles<br />No Russian subtitles have been submitted");
	} else if (localEnSubsSubmitted == false && localRuSubsSubmitted == true) {
		setInfoDivText("localVideoInfoDiv", "Processing Russian subtitles, this may take several seconds<br />No English subtitles have been submitted");
	} else if (localEnSubsSubmitted == false && localRuSubsSubmitted == false) {
		setInfoDivText("localVideoInfoDiv", "No subtitles have been submitted");
	} else {
		setInfoDivText("localVideoInfoDiv", "Processing subtitles . . .");
	}

	// Hide the options buttons
	var uploadButtons = document.getElementById("localUploadOptions")
	uploadButtons.style.opacity = 0;
	uploadButtons.style.display = "none";


	// Set and reveal video
	if (document.getElementById("localVideoContainer") != undefined) {
		document.getElementById("localVideoContainer").parentNode.removeChild(document.getElementById("localVideoContainer"));
	}


	var videoTag = document.createElement("video");
	videoTag.setAttribute("id", "localVideoContainer");
	videoTag.setAttribute("class", "centralComponent fade-in");
	videoTag.setAttribute("controls", "controls")
	document.getElementById("localVideoContent").prepend(videoTag);

	var localVideoContent = document.getElementById("localVideoContent");
	var localVideoContainer = document.getElementById("localVideoContainer");
	var localVideoSource = document.createElement("source");
	localVideoContainer.appendChild(localVideoSource);

	// Set video
	if (uploadedVideo.name.endsWith("mp4")) {
		localVideoSource.setAttribute("type", "video/mp4");
	} else if (uploadedVideo.name.endsWith("ogg")) {
		localVideoSource.setAttribute("type", "video/ogg");
	} else {
		localVideoSource.setAttribute("type", "video/webm");
	}

	// Get video URL
	localVideoSource.setAttribute("src", URL.createObjectURL(uploadedVideo));
	localVideoSource.setAttribute("id", "localVideoSource");


	// Reveal content
	localVideoContent.style.display = "block";
	localVideoContent.style.opacity = 1;


	// Reveal filter fields
	var elementsToUnhide = ["localVideoContainer", "localVideoContent", "localVideoFilterFieldLabel", "localVideoFilterField", "localVideoOptions"];
	for (element of elementsToUnhide) {
		var el = document.getElementById(element);
		el.style.display = "block";
		el.style.opacity = 1;
	}

	// Reveal InfoDIv
	var infoDiv = document.getElementById("localVideoInfoDiv");
	infoDiv.style.display = "flex";
	infoDiv.style.opacity = 1;

	// Local video player paused
	document.getElementById("localVideoContainer").onpause = function() {
		for (entry of localVideoInterval) {
			clearInterval(entry);
		}

		// Display any local subtitles
		var localSubs = getLocalSubtitles();

		var groupCaptureRegex = /([а-яА-ЯёЁ]+-{0,1}[а-яА-ЯёЁ]{0,1})/;
		var ruWords = [];
		for (word of localSubs.split(" ")) {
			var grabbedWord = groupCaptureRegex.exec(word);
			if (grabbedWord) {
				ruWords.push(grabbedWord[0].toLowerCase());
			}
		}

		// filter words on pause
		filterWords(ruWords, "localVideoWordListContainer", false);

		// Display subtitles	
		setInfoDivText("localVideoInfoDiv", localSubs);

	}

	// Local video player playing
	document.getElementById("localVideoContainer").onplay = function() {
		localVideoInterval.push(setInterval(getAndDisplayLocalSubtitles, 100));
	}

	// Organise Layout
	setLayout(false, false, false, "localTab");
}

function clearUploads() {
	document.getElementById("localVideoUpload").value = null;
	document.getElementById("localVideoRuSubsUpload").value = null;
	document.getElementById("localVideoEnSubsUpload").value = null;
}


function processYoutubeVideo() {

	// Get YouTube Link
	var link = document.getElementById("youtubeLinkField").value;

	// Exit early if no link present
	if (link.length == 0 || !(link.startsWith("https://www.youtube.com/watch?v") || link.startsWith("https://m.youtube.com/watch?v"))) {
		alert("Please enter a valid Youtube Link");
		return;
	} else if (link.indexOf("&list=") > -1) {
		alert("Unfortunately playlists are not currently supported");
		return;
	}

	youtubeVideoCode = link.substring(link.indexOf("=") + 1);


	// Fixes link if the channel data is tacked on the end
	if (youtubeVideoCode.indexOf("&") > -1) {
		youtubeVideoCode = youtubeVideoCode.substring(0, youtubeVideoCode.indexOf("&"));
	}

	// Append to link for russian subs
	// link = link + "?hl=en&amp;cc_lang_pref=ru&amp;cc=1";

	// Prepare the Youtube iframe
	if (!playerInitialised) {
		var tag = document.createElement('script');
		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	} else {
		player.cueVideoById(youtubeVideoCode);
	}


	// Hide fields to get more screen space for video
	var youtubeFields = document.getElementById("youtubeTabFields");
	youtubeFields.style.opacity = 0;
	youtubeFields.style.display = "none";
	youtubeFields.style.height = "0px";

	// Set and reveal iframe
	var iframeContainer = document.getElementById("iframeContainer");
	iframeContainer.style.display = "block";
	iframeContainer.style.opacity = 1;

	var heading = document.getElementById("youtubeFilterHeading");
	heading.style.display = "block";
	heading.style.opacity = 1;

	var headingText = document.getElementById("youtubeFilterFieldLabel");
	headingText.style.display = "block";
	headingText.style.opacity = 1;

	var filter = document.getElementById("youtubeFilterField");
	filter.style.display = "block";
	filter.style.opacity = 1;

	// Reveal buttons and infoDiv
	var chooseAnotherVideoButton = document.getElementById("youtubeOptions");
	chooseAnotherVideoButton.style.display = "block";
	chooseAnotherVideoButton.style.opacity = 1;

	var youtubeInfoDiv = document.getElementById("youtubeInfoDiv");
	youtubeInfoDiv.style.display = "flex";
	youtubeInfoDiv.style.opacity = 1;

	// Confirm layout change
	youtubeVideoDisplayed = true;

	// Organise Layout
	// Needed to display InfoDiv in case of error
	setLayout(youtubeEnlarged, youtubeAspectRatio, false, "youtubeTab");

	// Set text
	setInfoDivText("youtubeInfoDiv", "Video loaded, acquiring subtitles");

	// Subtitles URL
	// https://video.google.com/timedtext?lang=ru&v=Mc9UYBYEGZQ
	//sendAjax("POST", "https://video.google.com/timedtext?lang=en&v=Mc9UYBYEGZQ", null);
	sendAjax("POST", "https://video.google.com/timedtext?lang=en&v=" + youtubeVideoCode, null);
	ajaxPurpose = "ensubs";
	ajaxInfoDiv = "youtubeInfoDiv";
}

/*
For unknown reasons setting the video ID at this point prevents any events from firing.
To combat this the id is set during the onPlayerReady function, which loads once the video
player is set up 
*/
function onYouTubeIframeAPIReady() {
	player = new YT.Player('videoIframe', {
		playerVars: {
			//cc_load_policy: 1,
			//cc_lang_pref: 'ru,
			//origin: 'http://localhost:8100',
		},
		//		  videoId: 'Mc9UYBYEGZQ?hl=en&amp;cc_lang_pref=ru&amp;cc=1',
		events: {
			'onStateChange': onPlayerStateChange,
			'onReady': onPlayerReady
		}
	});
}

/* Set the id of the video */
function onPlayerReady(/*event*/) {

	//player.cueVideoById('zIwLWfaAg-8');
	player.cueVideoById(youtubeVideoCode);

	// Confirm player is now up and ready
	// It shouldn't need to be reinitialised
	playerInitialised = true;

}

// Called when the video changes state, needed for pause action
function onPlayerStateChange(event) {

	// Start
	if (event.data == 1) {
		interval.push(setInterval(getAndDisplaySubtitles, 100));
		//console.log("START: " + interval);
	}

	// Paused
	if (event.data == 2) {
		//console.log("PAUSED: " + interval);

		for (entry of interval) {
			clearInterval(entry);
		}

		setInfoDivText("youtubeInfoDiv", getCurrentSubtitles(true));
	}
}

function getAndDisplaySubtitles() {
	setInfoDivText("youtubeInfoDiv", getCurrentSubtitles(false))
}




function getCurrentSubtitles(filter) {

	// IF no subtitles have been received, return message 
	if (!russianSubsReceived && !englishSubsReceived) {
		return "No subtitles to display";
	}

	if (russianSubsReceived) {
		var russianXMLTimes = youtubeRussianSubsXML.getElementsByTagName("text");

		var subtitlesToReturn = "";
		var targetTime = player.getCurrentTime();

		// Handle delay changes by the user
		if (youtubeDelay > 0) { targetTime = targetTime - youtubeDelay };
		if (youtubeDelay < 0) { targetTime = targetTime + youtubeDelay };


		// Iterate over subtitles
		for (i = 0; i < russianXMLTimes.length; i++) {
			var startTime = +russianXMLTimes[i].getAttribute('start');
			var endTime = +startTime + +russianXMLTimes[i].getAttribute('dur');

			// If there are subtitles with times matching the current play time
			if (targetTime < endTime && targetTime > startTime) {
				var ruSubs = russianXMLTimes[i].childNodes[0].nodeValue;

				// If the button has been pushed, then we want to colour the relevant verbs
				if (document.getElementById("youtubeColourPerfectives").value == "Decolour Perfective Verbs") {
					//Decolour Perfective Verbs
					var splitSubs = ruSubs.replace(/\n/g, " ").split(" ");

					for (word of splitSubs) {

						var match = regexpNonGlobal.exec(word);
						if (match != null) {
							var matchedWord = match[0]
							if (youtubePerfectiveVerbs.includes(matchedWord.toLowerCase())) {
								var colouredText = word.replace(matchedWord, '<span style="color: #179757">' + matchedWord + '</span>');
								subtitlesToReturn = subtitlesToReturn + colouredText + " ";
							} else if (youtubePerfectiveVerbsMultipleDefinitions.includes(matchedWord.toLowerCase())) {
								var colouredText = word.replace(matchedWord, '<span style="color: #f0544f">' + matchedWord + '</span>');
								subtitlesToReturn = subtitlesToReturn + colouredText + " ";
							} else {
								subtitlesToReturn = subtitlesToReturn + word + " ";
							}
						} else {
							subtitlesToReturn = subtitlesToReturn + word + " ";
						}
					}


				} else {
					// Create text to return
					subtitlesToReturn = subtitlesToReturn + ruSubs.replace(/\n/g, " ");
				}


				if (filter === true) {
					//var regexp = /[а-яА-ЯёЁ]+-{0,1}[а-яА-ЯёЁ]{0,1}/g;
					//var words = [...subs.matchAll(regexp)];

					var words = [];
					while ((result = regexp.exec(ruSubs)) !== null) {
						words.push(String(result).toLowerCase());
					}

					// Filter the word list with the subtitled words
					filterWords(words, "youtubeWordListContainer", false);
				}
				break;
			}
		}

		//subtitlesToReturn =  subtitlesToReturn + "<hr/>English: ";
		subtitlesToReturn = subtitlesToReturn + "<br /><br />";
	}


	if (englishSubsReceived) {

		var englishXMLTimes = youtubeEnglishSubsXML.getElementsByTagName("text");

		for (i = 0; i < englishXMLTimes.length; i++) {
			var startTime = +englishXMLTimes[i].getAttribute('start');
			var endTime = +startTime + +englishXMLTimes[i].getAttribute('dur');

			// If there are subtitles with times matching the current play time
			if (targetTime < endTime && targetTime > startTime) {
				var enSubs = englishXMLTimes[i].childNodes[0].nodeValue;

				// Create text to return
				subtitlesToReturn = subtitlesToReturn + enSubs.replace(/\n/g, " ");
				break;
			}
		}
	}
	return subtitlesToReturn;
}

function chooseAnotherVideo(target) {

	if (target == "youtube") {
		// Hide the iframe container
		var iframeContainer = document.getElementById("iframeContainer");
		iframeContainer.style.opacity = 0;
		iframeContainer.style.display = "none";

		// Reveal the original selection fields
		var youtubeFields = document.getElementById("youtubeTabFields");
		youtubeFields.style.opacity = 1;
		youtubeFields.style.display = "block";
		youtubeFields.style.height = "300px";

		// Remove the list of words, ready for another
		// Check first as it may not always be present
		if (document.body.contains(document.getElementById("youtubeUL"))) {
			document.getElementById("youtubeUL").parentNode.removeChild(document.getElementById("youtubeUL"));
		}

		// Hide elements to clear screen
		additionalElementsToHide = ["youtubeOptions",
			"youtubeInfoDiv", "youtubeFilterFieldLabel",
			"youtubeFilterField"];

		for (i = 0; i < additionalElementsToHide.length; i++) {
			var element = document.getElementById(additionalElementsToHide[i]);
			element.style.display = "none";
			element.style.opacity = "0";
		}


		// Hide list container and remove scroll bars, otherwise they remain 
		var youtubeWordListContainer = document.getElementById("youtubeWordListContainer")
		//youtubeWordListContainer.style.overflowY = "hidden";
		youtubeWordListContainer.style.height = "";

		// Clear any remaining intervals
		for (entry of interval) {
			clearInterval(entry);
		}

		// Clear variables ready for next run
		russianSubsReceived = false;
		englishSubsReceived = false
		youtubeEnglishSubsXML = "";
		youtubeRussianSubsXML = "";
		youtubePerfectiveVerbs = [];
		youtubeVideoDisplayed = false;
		youtubeTranslations = [];
		//document.getElementById("youtubeDisplayTranslations").value = "Display List Translations";
		//document.getElementById("youtubeColourPerfectives").value = "Colour Perfective Verbs";


		// Stop and clear video player
		player.pauseVideo();
		player.cueVideoById("");

		// Focus ready for next link
		var linkField = document.getElementById("youtubeLinkField");
		linkField.value = "";
		linkField.focus();

	} else {//Local Video

		var localVideoFields = document.getElementById("localUploadOptions");
		localVideoFields.style.opacity = 1;
		localVideoFields.style.display = "block";
		localVideoFields.style.height = "50%";

		// Check first as it may not always be present
		if (document.body.contains(document.getElementById("localUL"))) {
			document.getElementById("localUL").parentNode.removeChild(document.getElementById("localUL"));
		}

		// Hide other elements
		additionalElementsToHide = ["localVideoContainer", "localVideoOptions",
			"localVideoInfoDiv", "localVideoFilterFieldLabel", "localVideoFilterField"];

		for (i = 0; i < additionalElementsToHide.length; i++) {
			var element = document.getElementById(additionalElementsToHide[i]);
			element.style.opacity = "0";
			element.style.display = "none";
		}

		// Resets the height for the waterfall effect
		document.getElementById("localVideoWordListContainer").style.height = "0px";

		// Clear any remaining intervals
		for (entry of localVideoInterval) {
			clearInterval(entry);
		}

		// Clear variables
		localRuSubsFileContent = "";
		localEnSubsFileContent = "";
		localRuSubsProcessed = false;
		localEnSubsProcessed = false;
		parsedLocalRuSubs = [];
		parsedLocalEnSubs = [];
		localRuSubsSubmitted = false;
		localEnSubsSubmitted = false;
		localPerfectiveVerbs = [];
		localPerfectiveVerbsMultipleDefinitions = [];
		localTranslations = [];
		//document.getElementById("localVideoDisplayTranslations").value = "Display List Translations";
		//document.getElementById("localVideoColourPerfectives").value = "Colour Perfective Verbs";



		// Remove video
		document.getElementById("localVideoContainer").pause();
		document.getElementById("localVideoSource").parentNode.removeChild(document.getElementById("localVideoSource"));
		document.getElementById("localVideoContainer").parentNode.removeChild(document.getElementById("localVideoContainer"));
		
		// Remove previous entries
		clearUploads();
		
		// Focus to enter a new video
		document.getElementById("localVideoUpload").focus();

	}
}

function filterWords(words, container, userTyped) {

	// Prevent keypress if accessibility jump to filter 
	if (accessibilityJumpToFilter == true) {
		if (accessibilityJumpCount == 0) {
			accessibilityJumpCount++;
			return;
		} else {
			accessibilityJumpToFilter = false;
			return;

		}
	}

	// Prevent list resetting when just tabbing to it 
	if (words == "") {
		return;
	}




	if (words == "clear" || words == " ") {
		words = ""
		document.getElementById(document.activeElement.id).value = "";
	}

	// Declare variables
	var ul, li, i, txtValue;
	//input = document.getElementById('filterField');

	//filter = input.value.toLowerCase();
	ul = document.getElementById(container);
	li = ul.getElementsByTagName('li');

	// Empty, reset filter to show all words
	if (words.length == 0) {
		for (i = 0; i < li.length; i++) {
			li[i].style.display = "";
		}

		// words to filter
	} else {
		for (i = 0; i < li.length; i++) {
			txtValue = li[i].children[0].id;
			if (userTyped) {
				if (txtValue.toLowerCase().indexOf(words) > -1) {
					li[i].style.display = "";
				} else {
					li[i].style.display = "none";
				}
			} else if (words.includes(txtValue.toLowerCase())) {
				li[i].style.display = "";
			} else {
				li[i].style.display = "none";
			}
		}
	}
}


function createListFromJson(words, wordList) {

	for (searchWord in words) {//Get array of words
		var newListEntry = document.createElement("li");
		newListEntry.tabIndex = 0;

		for (bareWord in words[searchWord]) {//All data associated with a word
			//		document.writeln("Word: " + bareWord + "<br />");

			var newWordEntry = document.createElement("div");
			newWordEntry.setAttribute("class", "wordEntry");
			newWordEntry.setAttribute("id", bareWord);
			newWordEntry.setAttribute("onclick", "displayTable(event, this.id)");
			newListEntry.appendChild(newWordEntry);

			var newHeaderWord = document.createElement("div");
			newHeaderWord.setAttribute("class", "headerWord disable-select");
			newHeaderWord.innerText = bareWord;
			newWordEntry.appendChild(newHeaderWord);

			var newWordFormsData = document.createElement("div");
			newWordFormsData.setAttribute("class", "wordFormsData");
			newWordEntry.appendChild(newWordFormsData);

			var newTab = document.createElement("div");
			newTab.setAttribute("class", "tab");
			newWordFormsData.appendChild(newTab);


			var wordCount = 1;
			for (type in words[searchWord][bareWord]) {//Get array of word types (e.g nouns, verbs)
				for (typeEntry in words[searchWord][bareWord][type]) {//Get each type
					var verbCount = 0;
					for (entry in words[searchWord][bareWord][type][typeEntry]) {//Each word definition within the type
						var newButton = document.createElement("button");
						if (wordCount === 1) {
							newButton.setAttribute("class", "tablinks active");
						} else {
							newButton.setAttribute("class", "tablinks");
						}

						newButton.setAttribute("onclick", "openTab(event, '" + bareWord + "/" + wordCount + "', this)");
						newTab.appendChild(newButton);

						var newTabContent = document.createElement("div");
						newTabContent.setAttribute("id", bareWord + "/" + wordCount);
						newTabContent.setAttribute("class", "tabContent");

						var newTable = document.createElement("table");

						var definition = words[searchWord][bareWord][type][typeEntry][entry];

						var newbuttonTextSet = false;

						var jsonKeys = Object.keys(definition);

						switch (typeEntry) {
							case "nouns":

								var rowStartingNum = jsonKeys[0].substring(1, jsonKeys[0].indexOf("_"));
								var tabHeading = definition["W" + rowStartingNum + "_accented"];

								var headingSpan = document.createElement("span")
								headingSpan.setAttribute("class", "wordChoice");
								headingSpan.appendChild(document.createTextNode(tabHeading))
								newButton.appendChild(headingSpan);
								//newButton.innerText = tabHeading;

								var tableTitle = document.createElement("h3");
								tableTitle.innerHTML = "<u>Noun</u><br />" + tabHeading;
								newTabContent.appendChild(tableTitle);

								var mainSeparatorHeadings = ["Singular", "Plural", "Notes", "Translation"];
								var declensionsHeadings = ["Nominative", "Accusative", "Genitive", "Dative", "Instrumental", "Prepositional"];
								var declensionsValues = ["nom", "acc", "gen", "dat", "inst", "prep"];
								var notesHeadings = ["Animate", "Indeclinable", "Gender", "Singular Only", "Plural only"];
								var notesValues = ["animate", "indeclinable", "gender", "sg_only", "pl_only"];

								var firstDeclensionsRow = document.createElement("tr");
								var secondDeclensionsRow = document.createElement("tr");
								var firstDeclensionHeadingsRow = document.createElement("tr");
								var secondDeclensionHeadingsRow = document.createElement("tr");

								var notesHeadingsRow = document.createElement("tr");
								var notesRow = document.createElement("tr");

								var cell;

								for (var i = 0; i < declensionsHeadings.length; i++) {
									cell = document.createElement("td");
									setText(cell, definition["W" + rowStartingNum + "_" + declensionsValues[i]], 1, false)
									firstDeclensionsRow.appendChild(cell);

									cell = document.createElement("td");
									setText(cell, definition["W" + (+rowStartingNum + 1) + "_" + declensionsValues[i]], 1, false)
									secondDeclensionsRow.appendChild(cell);

									cell = document.createElement("th");
									setText(cell, declensionsHeadings[i], 1, false);
									firstDeclensionHeadingsRow.appendChild(cell);

									cell = document.createElement("th");
									setText(cell, declensionsHeadings[i], 1, false);
									secondDeclensionHeadingsRow.appendChild(cell);

									//Notes will have 1 less heading, so skip the final iteration from here
									if (i === declensionsHeadings.length - 1) {
										continue;
									}

									//Gender needs to span 2.  i will equal 2 when on the gender heading
									var colspan = (i === 2 ? 2 : 1);

									cell = document.createElement("th");
									setText(cell, notesHeadings[i], colspan, false);
									notesHeadingsRow.appendChild(cell);

									cell = document.createElement("td");
									setText(cell, definition["W" + rowStartingNum + "_" + notesValues[i]], colspan, false);
									notesRow.appendChild(cell);
								}

								var singularOnly = definition["W" + rowStartingNum + "_" + "sg_only"];
								var pluralOnly = definition["W" + rowStartingNum + "_" + "pl_only"];

								var newRow = document.createElement("tr");
								if (pluralOnly == 0) {
									newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[0]));//Singular
									appender(newTable, newRow, firstDeclensionHeadingsRow, firstDeclensionsRow);
								}

								if (singularOnly == 0) {
									newRow = document.createElement("tr");
									newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[1]));//Plural
									appender(newTable, newRow, secondDeclensionHeadingsRow, secondDeclensionsRow);
								}

								newRow = document.createElement("tr");
								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[2]));//Notes
								appender(newTable, newRow, notesHeadingsRow, notesRow);

								newRow = document.createElement("tr");
								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[3]));//Translation
								newTable.appendChild(newRow);
								newRow = document.createElement("tr");
								cell = document.createElement("td");
								setText(cell, definition["Translation"], 6);

								newRow.appendChild(cell);
								newTable.appendChild(newRow);

								newTabContent.appendChild(newTable);
								newWordFormsData.appendChild(newTabContent);

								break;

							case "verbs":
								verbCount++;

								var rowStartingNum = jsonKeys[0].substring(1, jsonKeys[0].indexOf("_"));
								var tabHeading = definition["W" + rowStartingNum + "_accented"];
								var tabHeadingPrefix = (verbCount === 1 ? "" : "Partner ");

								var headingSpan = document.createElement("span")
								headingSpan.setAttribute("class", "wordChoice");
								headingSpan.appendChild(document.createTextNode(tabHeading))
								newButton.appendChild(headingSpan);
								//newButton.textContent = tabHeading;

								var tableTitle = document.createElement("h3");
								tableTitle.innerHTML = "<u>" + tabHeadingPrefix + "Verb</u><br />" + tabHeading;
								newTabContent.appendChild(tableTitle);


								var mainSeparatorHeadings = ["Present", "Past", "Imperative", "Notes", "Translation"];
								var presentHeadings = ["1st Person Singular | Я",
									"2nd Person Singular (Informal) | Ты",
									"2nd Person Plural (Formal) | Вы",
									"3rd Person Singular | Он/Она/Оно",
									"1st Person Plural | Мы",
									"3rd Person Plural | Они"];

								var pastHeadings = ["", "Masculine Singular | Я/Он/Ты", "Feminine Singular  | Я/Она/Ты", "Neuter | Оно", "Plural | Они/Мы/Вы", ""]
								var imperativeHeadings = ["", "", "Singular", "Plural", "", ""]
								var presentValues = ["sg1", "sg2", "pl2", "sg3", "pl1", "pl3"];
								var pastValues = ["", "past_m", "past_f", "past_n", "past_pl", ""];
								var imperativeValues = ["", "", "imperative_sg", "imperative_pl", "", ""];
								var notesHeadings = ["", "", "Aspect", "Partner(s)", "", ""];
								var notesValues = ["", "", "aspect", "partner", "", ""];

								var presentHeadingsRow = document.createElement("tr");
								var pastHeadingsRow = document.createElement("tr");
								var imperativeHeadingsRow = document.createElement("tr");
								var notesHeadingsRow = document.createElement("tr");

								var presentValuesRow = document.createElement("tr");
								var pastValuesRow = document.createElement("tr");
								var imperativeValuesRow = document.createElement("tr");
								var notesValuesRow = document.createElement("tr");

								var cell;
								for (var i = 0; i < presentHeadings.length; i++) {
									cell = document.createElement("th");
									setText(cell, presentHeadings[i], 1, false);
									presentHeadingsRow.appendChild(cell);

									cell = document.createElement("td");
									setText(cell, definition["W" + rowStartingNum + "_" + presentValues[i]], 1, false);
									presentValuesRow.appendChild(cell);

									cell = document.createElement("th");
									setText(cell, pastHeadings[i], 1, false);
									pastHeadingsRow.appendChild(cell);

									cell = document.createElement("td");
									setText(cell, definition["W" + rowStartingNum + "_" + pastValues[i]], 1, false);
									pastValuesRow.appendChild(cell);

									cell = document.createElement("th");
									setText(cell, imperativeHeadings[i], 1, false);
									imperativeHeadingsRow.appendChild(cell);

									cell = document.createElement("td");
									setText(cell, definition["W" + rowStartingNum + "_" + imperativeValues[i]], 1, false);
									imperativeValuesRow.appendChild(cell);

									//Routine that changes the heading Partner(s) to either Partner or Partners, depending on which is appropriate 
									cell = document.createElement("td");
									if (i === 3) {
										var partnersValues = definition["W" + rowStartingNum + "_" + notesValues[i]];
										var multiplePartners = (partnersValues.indexOf(";") > -1 || partnersValues.indexOf(",") > -1 ? true : false);

										if (multiplePartners) {
											setText(cell, "Partners", 1, false);
										} else {
											setText(cell, "Partner", 1, false);
										}
									} else {
										cell = document.createElement("td");
										setText(cell, notesHeadings[i], 1, false);
									}
									notesHeadingsRow.appendChild(cell);


									notesHeadingsRow.appendChild(cell);
									cell = document.createElement("td");
									setText(cell, definition["W" + rowStartingNum + "_" + notesValues[i]], 1, false);
									notesValuesRow.appendChild(cell);
								}


								var newRow = document.createElement("tr");

								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[0]));
								appender(newTable, newRow, presentHeadingsRow, presentValuesRow);
								newRow = document.createElement("tr");

								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[1]));
								appender(newTable, newRow, pastHeadingsRow, pastValuesRow);
								newRow = document.createElement("tr");

								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[2]));
								appender(newTable, newRow, imperativeHeadingsRow, imperativeValuesRow);
								newRow = document.createElement("tr");

								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[3]));
								appender(newTable, newRow, notesHeadingsRow, notesValuesRow);
								newRow = document.createElement("tr");

								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[4]));
								cell = document.createElement("td");
								setText(cell, definition["Translation"], 6);
								appender(newTable, newRow, cell)

								newTabContent.appendChild(newTable);
								newWordFormsData.appendChild(newTabContent);

								break;


							case "adjectives":

								var rowStartingNum = jsonKeys[0].substring(1, jsonKeys[0].indexOf("_"));
								var tabHeading = definition["W" + rowStartingNum + "_accented"];

								var headingSpan = document.createElement("span")
								headingSpan.setAttribute("class", "wordChoice");
								headingSpan.appendChild(document.createTextNode(tabHeading))
								newButton.appendChild(headingSpan);
								//newButton.innerText = tabHeading;

								var tableTitle = document.createElement("h3");
								tableTitle.innerHTML = "<u>Adjective</u><br />" + tabHeading;
								newTabContent.appendChild(tableTitle);

								var mainSeparatorHeadings = ["Masculine", "Feminine", "Neuter", "Plural", "Comparative, Superlative & Short", "Translation"];
								var mainHeadings = ["Nominative", "Accusative (Animate)", "Genitive", "Dative", "Instrumental", "Prepositional"];
								var shortHeadings = ["Comparative", "Superlative", "Short Masculine", "Short Feminine", "Short Neuter", "Short Plural"];

								var dataValues = ["nom", "acc", "gen", "dat", "inst", "prep"];
								var shortValues = ["comparative", "superlative", "short_m", "short_f", "short_n", "short_pl"];

								var masculineHeadingRow = document.createElement("tr");
								var feminineHeadingRow = document.createElement("tr");
								var neuterHeadingRow = document.createElement("tr");
								var pluralHeadingRow = document.createElement("tr");
								var shortHeadingRow = document.createElement("tr");

								var masculineValuesRow = document.createElement("tr");
								var feminineValuesRow = document.createElement("tr");
								var neuterValuesRow = document.createElement("tr");
								var pluralValuesRow = document.createElement("tr");
								var shortValuesRow = document.createElement("tr");

								var cell;
								for (var i = 0; i < mainHeadings.length; i++) {

									cell = document.createElement("th");
									setText(cell, mainHeadings[i], 1, false);
									masculineHeadingRow.appendChild(cell);

									cell = document.createElement("td");
									setText(cell, definition["W" + rowStartingNum + "_" + dataValues[i]], 1, true);
									masculineValuesRow.appendChild(cell);

									cell = document.createElement("th");
									setText(cell, (i === 1 ? "Accusative" : mainHeadings[i]), 1, false);//Ternary prevents animate heading being used
									feminineHeadingRow.appendChild(cell);

									cell = document.createElement("td");
									setText(cell, definition["W" + (+rowStartingNum + 1) + "_" + dataValues[i]], 1, false);
									feminineValuesRow.appendChild(cell);

									cell = document.createElement("th");
									setText(cell, (i === 1 ? "Accusative" : mainHeadings[i]), 1, false);//Ternary prevents animate heading being used
									neuterHeadingRow.appendChild(cell);

									cell = document.createElement("td");
									setText(cell, definition["W" + (+rowStartingNum + 2) + "_" + dataValues[i]], 1, false);
									neuterValuesRow.appendChild(cell);

									cell = document.createElement("th");
									setText(cell, mainHeadings[i], 1, false);
									pluralHeadingRow.appendChild(cell);

									cell = document.createElement("td");
									setText(cell, definition["W" + (+rowStartingNum + 3) + "_" + dataValues[i]], 1, true);
									pluralValuesRow.appendChild(cell);

									cell = document.createElement("th");
									setText(cell, shortHeadings[i], 1, false);
									shortHeadingRow.appendChild(cell);

									cell = document.createElement("td");
									setText(cell, definition["W" + (rowStartingNum) + "_" + shortValues[i]], 1, false);
									shortValuesRow.appendChild(cell);

								}

								var newRow = document.createElement("tr");

								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[0]));
								appender(newTable, newRow, masculineHeadingRow, masculineValuesRow);
								newRow = document.createElement("tr");

								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[1]));
								appender(newTable, newRow, feminineHeadingRow, feminineValuesRow);
								newRow = document.createElement("tr");

								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[2]));
								appender(newTable, newRow, neuterHeadingRow, neuterValuesRow);
								newRow = document.createElement("tr");

								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[3]));
								appender(newTable, newRow, pluralHeadingRow, pluralValuesRow);
								newRow = document.createElement("tr");

								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[4]));
								appender(newTable, newRow, shortHeadingRow, shortValuesRow);
								newRow = document.createElement("tr");

								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[5]));
								cell = document.createElement("td");
								setText(cell, definition["Translation"], 6)
								appender(newTable, newRow, cell);

								newTabContent.appendChild(newTable);
								newWordFormsData.appendChild(newTabContent);

								break;


							case "others":

								var rowStartingNum = jsonKeys[0].substring(1, jsonKeys[0].indexOf("_"));
								var tabHeading = definition["W" + rowStartingNum + "_accented"];

								var headingSpan = document.createElement("span")
								headingSpan.setAttribute("class", "wordChoice");
								headingSpan.appendChild(document.createTextNode(tabHeading))
								newButton.appendChild(headingSpan);
								//newButton.innerText = tabHeading;

								var tableTitle = document.createElement("h3");
								tableTitle.innerHTML = "<u>Other</u><br />" + tabHeading;
								newTabContent.appendChild(tableTitle);

								var newRow = document.createElement("tr");
								var cell = document.createElement("td");

								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", "Word"));
								setText(cell, tabHeading, 6);
								appender(newTable, newRow, cell);

								newRow = document.createElement("tr");
								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", "Translation"));

								cell = document.createElement("td");
								setText(cell, definition["Translation"], 6);
								appender(newTable, newRow, cell);

								newTabContent.appendChild(newTable);
								newWordFormsData.appendChild(newTabContent);

								break;

							default:
								var newHeadingsTableRow = document.createElement("tr");

								jsonKeys.forEach(function(key) {
									var newTableCell = document.createElement("td");
									newTableCell.textContent = definition[key];
									newHeadingsTableRow.appendChild(newTableCell);


									if (newbuttonTextSet === false) {
										if (key.endsWith("accented")) {
											newbuttonTextSet = true;
											newButton.innerText = definition[key];
										}
									}
								});

								newTable.appendChild(newHeadingsTableRow);
								newTabContent.appendChild(newTable);
								newWordFormsData.appendChild(newTabContent);
						}

						if (wordCount === 1) {
							newTabContent.style.display = "block";
						}

						wordCount++;
					}
				}
			}
		}
		wordList.appendChild(newListEntry);
	}
}

function getNewTableHeadingRow(columns, classAttribute, text) {
	var newRow = document.createElement("th");
	newRow.setAttribute("colspan", columns);
	newRow.setAttribute("class", classAttribute);
	newRow.textContent = text;

	return newRow;
}

function appender(parent) {
	for (var i = 1; i < arguments.length; i++) {
		parent.appendChild(arguments[i]);
	}
}

function setText(cell, text, colspan, animate) {
	if (text) {//Avoid "" or undefined
		switch (text) {
			case "m":
				text = "Masculine";
				break

			case "f":
				text = "Feminine";
				break

			case "n":
				text = "Neuter";
				break

			case "0":
				text = "No";
				break

			case "1":
				text = "Yes";
				break

			case "null":
				text = "Unknown";
				break;

			case "imperfective":
				text = text[0].toUpperCase() + text.substring(1);
				break

			case "perfective":
				text = text[0].toUpperCase() + text.substring(1);
				break

			default:
				text = text;
		}

		if (animate) {
			if (text.indexOf(",") > -1) {
				text = text.replace(",", " (");
				text = text + ")";
			}
		} else {
			if (text.indexOf(";") > -1 || text.indexOf(",") > -1) {
				text = text.replace(/;/g, ",");
				text = text.replace(/,/g, ", ");
			}
		}
	}

	cell.textContent = text;

	if (colspan > 1) {
		cell.setAttribute("colspan", colspan);
	}
}



function displayTable(event, id) {

	var eventTarget = false;
	if (event.target.className === "header"
		|| event.target.className === "wordEntry"
		|| event.target.parentElement.className === "wordEntry") {

		eventTarget = true;
	}

	//Detects it the tabs' container has been selected
	if (eventTarget) {

		/////Locks so that only 1 word entry can be displayed at any one time
		var displayOnlyOne = true;

		var searchBlock = document.getElementById(id).children[1];


		//	Keep list elements coloured after selection
		var header = searchBlock.parentElement.children[0];
		var selectedContainer = document.getElementById(searchBlock.parentElement.id);



		//Displays|hides the tabs when their container is selected
		if (searchBlock.style.display === "") {
			searchBlock.style.display = "block";


			if (displayOnlyOne) {
				//Clear colours on all words before setting one
				var allHeaderwords = document.querySelectorAll(".headerWord");
				var allWordEntrys = document.querySelectorAll(".wordEntry");

				for (var i = 0; i < allHeaderwords.length; i++) {
					allHeaderwords[i].style.backgroundColor = "";
					allWordEntrys[i].style.backgroundColor = "";
				}
			}

			//	Keep list elements coloured after selection
			header.style.backgroundColor = "#C3073F";
			selectedContainer.style.backgroundColor = "#C3073F";


			// Scrolls table into view to allow viewing of all the data
			selectedContainer.scrollIntoView(true);


		} else {
			//	Clear element colouring after selection
			searchBlock.style.display = "";

			header.style.backgroundColor = "";
			selectedContainer.style.backgroundColor = "";
		}


		if (displayOnlyOne) {
			var entries = document.getElementsByClassName("wordEntry");
			for (var i = 0; i < entries.length; i++) {
				var entryId = entries[i].id;

				if (!(entryId === searchBlock.parentElement.id)) {
					entries[i].children[1].style.display = "";
				}
			}
		}
	}
}

//Open & Close selected tab data, used in word lists
function openTab(evt, tab, element) {

	var i, tabContent, tablinks;
	tabContent = element.parentElement.parentElement.getElementsByClassName("tabContent");

	for (i = 0; i < tabContent.length; i++) {
		tabContent[i].style.display = "none";
	}

	tablinks = element.parentElement.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}

	document.getElementById(tab).style.display = "block";
	evt.currentTarget.className += " active";
}

//Open & Close selected nav tab data, used in option navigation
function openNavTab(evt, selectedNavTab) {
	var i, navTabcontent, navTablinks;
	navTabcontent = document.getElementsByClassName("navTabcontent");

	for (i = 0; i < navTabcontent.length; i++) {
		navTabcontent[i].style.display = "none";
	}

	navTablinks = document.getElementsByClassName("navTablinks");
	for (i = 0; i < navTablinks.length; i++) {
		navTablinks[i].className = navTablinks[i].className.replace(" active", "");
	}
	document.getElementById(selectedNavTab).style.display = "block";
	evt.currentTarget.className += " active";

	// Expand selected tab
	resizeNavTabContent();

	// Redo the layout to ensure it is correct
	// "resize" causes the layout to detect variables, preserving apsect ratios and enlarged video
	setLayout("resize");
}

function goToTab(tab) {

	switch (tab) {

		case "youtube":
			document.getElementById("youtubeTabLink").click();
			document.getElementById("youtubeLinkField").focus({ preventScroll: true });
			break;

		case "local":
			document.getElementById("localTabLink").click();
			document.getElementById("localVideoUploadLabel").focus({ preventScroll: true });
			break;

		case "dictionary":
			document.getElementById("dictionaryTabLink").click();
			document.getElementById("dictionaryTextArea").focus({ preventScroll: true });
			break;
	}
}

function toggleKeyBindings() {

	var requestButton = document.getElementById("keybindingsButton");

	if (keyBindingsOn == false) {
		requestButton.style.backgroundColor = "#1793D1";
		requestButton.value = "Turn off accessibility keybindings: ON";
		keyBindingsOn = true;
	} else {
		requestButton.style.backgroundColor = "#179757";
		requestButton.value = "Turn on accessibility keybindings: OFF";
		keyBindingsOn = false;
	}
}

document.addEventListener('keydown', function(passedEvent) {

	/*
	This code detects a spacebar press on a list element and simulates a mouseclick on it.
	This enables browsing the results via a keyboard
	It is applied window wide to block the spacebar scrolling the window    
	*/
	if (passedEvent.code == "Space" && document.activeElement.tagName === "LI") {
		passedEvent.preventDefault();
		passedEvent.target.children[0].click();
	}

	// Colours buttons on spacebar keypress enter
	if (passedEvent.code == "Space" && document.activeElement.type === "button") {
		document.activeElement.style.backgroundColor = "#C3073F";
	}

	if (passedEvent.code == "Backspace") {
		var currentElement = document.activeElement;
		if (currentElement.name.endsWith("FilterField")) {
			if (currentElement.value.length == 0) {
				switch (currentElement.name) {
					case "youtubeFilterField":
						filterWords("clear", "youtubeWordListContainer", true);
						break;

					case "localVideoFilterField":
						filterWords("clear", "localVideoWordListContainer", true);
						break;

					case "dictionaryFilterField":
						filterWords("clear", "dictionaryWordListContainer", true);
						break;
				}

			}
		};
	}


	if (passedEvent.ctrlKey || keyBindingsOn == false
		|| document.activeElement.name == "youtubeLinkField" || document.activeElement.name == "dictionaryTextArea") {
		return;
	}


	/* KEY BINDINGS */
	var tab = getActiveNavTab();


	/* DETECT SHIFT  D */
	if (passedEvent.code == "KeyD" && passedEvent.shiftKey) {
		passedEvent.preventDefault();
		document.getElementById("dictionaryTabLink").click();
		document.getElementById("dictionaryTextArea").focus({ preventScroll: true });
	}

	/* DETECT SHIFT  I */
	if (passedEvent.code == "KeyI" && passedEvent.shiftKey) {
		passedEvent.preventDefault();
		document.getElementById("settingsTabLink").click();
	}

	/* DETECT SHIFT L */
	if (passedEvent.code == "KeyL" && passedEvent.shiftKey) {
		passedEvent.preventDefault();
		document.getElementById("localTabLink").click();
		document.getElementById("localVideoUploadLabel").focus({ preventScroll: true });
	}

	/* DETECT SHIFT M */
	if (passedEvent.code == "KeyM" && passedEvent.shiftKey) {
		passedEvent.preventDefault();
		document.getElementById("donateTabLink").click();
	}


	/* DETECT SHIFT  O */
	if (passedEvent.code == "KeyO" && passedEvent.shiftKey) {
		passedEvent.preventDefault();

		switch (tab) {
			case "youtubeTab":
				document.getElementById("chooseAnotherYoutubeVideo").focus();
				break;

			case "localTab":
				document.getElementById("chooseAnotherLocalVideo").focus();
				break;

			case "dictionaryTab":
				document.getElementById("dictionarySubmit").focus();
				break;
		}
	}

	/* DETECT SHIFT  P */
	if (passedEvent.code == "KeyP" && passedEvent.shiftKey) {
		passedEvent.preventDefault();

		switch (tab) {
			case "youtubeTab":
				if (player) {
					if (player.getPlayerState() == 1) {
						player.pauseVideo();
					} else {
						player.playVideo();
					}
				}
				break;

			case "localTab":
				var localPlayer = document.getElementById("localVideoContainer");
				if (localPlayer.paused == true) {
					localPlayer.play();
				} else {
					localPlayer.pause();
				}
				break;
		}
	}


	/* DETECT SHIFT S */
	if (passedEvent.code == "KeyS" && passedEvent.shiftKey) {
		passedEvent.preventDefault();

		accessibilityJumpToFilter = true;

		switch (tab) {
			case "youtubeTab":
				document.getElementById("youtubeFilterField").select();
				break;

			case "localTab":
				document.getElementById("localVideoFilterField").select();
				break;

			case "dictionaryTab":
				document.getElementById("dictionaryFilterField").select();
				break;

			case "settingsTab":
				document.getElementById("keybindingsButton").focus();
				break;
		}

	}

	/* DETECT SHIFT Y */
	if (passedEvent.code == "KeyY" && passedEvent.shiftKey) {
		passedEvent.preventDefault();
		document.getElementById("youtubeTabLink").click();
		if (youtubeVideoDisplayed == false) {
			document.getElementById("youtubeLinkField").select();
		}
	}

	/* DETECT LEFT ARROW */
	if (passedEvent.code == "ArrowLeft" && passedEvent.shiftKey) {
		passedEvent.preventDefault();
		switch (tab) {
			case "youtubeTab":
				player.playtime = player.seekTo(player.getCurrentTime() - 5);
				break;


			case "localTab":
				var localPlayer = document.getElementById("localVideoContainer");
				localPlayer.currentTime = localPlayer.currentTime - 5;
				break;
		}
	}

	/* DETECT RIGHT ARROW */
	if (passedEvent.code == "ArrowRight" && passedEvent.shiftKey) {
		passedEvent.preventDefault();
		switch (tab) {
			case "youtubeTab":
				player.playtime = player.seekTo(player.getCurrentTime() + 5);
				break;

			case "localTab":
				var localPlayer = document.getElementById("localVideoContainer");
				localPlayer.currentTime = localPlayer.currentTime + 5;
				break;
		}
	}
});

document.addEventListener('keyup', function(passedEvent) {

	// Resets colour background on buttons
	if (passedEvent.code == "Space" && document.activeElement.type === "button") {
		document.activeElement.style.backgroundColor = "hsl(150, 100%, 50%, 0.5)";
	}
});

/* Code to resize the nav tabs and make them fit the screen as it changes */
function resizeNavTabContent() {


	var screenHeight = (screen.width <= 1200) ? document.body.scrollHeight : window.innerHeight;
	var navTabs = document.getElementById("navTabContainer");

	//var viewportHeight = window.innerHeight;
	//var viewportHeight = document.body.scrollHeight;

	var navTabContent;
	var space;
	var newHeight;



	switch (getActiveNavTab()) {
		case "youtubeTab":
			navTabContent = document.getElementById("youtubeTabContent");
			break;

		case "localTab":
			navTabContent = document.getElementById("localVideoTabContent");
			break;


		case "dictionaryTab":
			navTabContent = document.getElementById("dictionaryTabContent");
			break;

		case "donateTab":
			navTabContent = document.getElementById("donateTabContent");
			break;


		case "settingsTab":
			navTabContent = document.getElementById("settingsTabContent");
			break;

	}


	space = screenHeight - (navTabContent.offsetTop + navTabContent.offsetHeight);
	newHeight = (space + (navTabContent.offsetHeight - navTabContent.offsetTop) + navTabs.offsetHeight) - 15;
	navTabContent.style.height = newHeight + "px";
}



// Detects which navTab is currently open
// Used to determine which tabs requires a layout update 
function getActiveNavTab() {
	var tabs = document.getElementsByClassName("navTablinks");
	for (navTab of tabs) {
		if (navTab.classList.contains("active")) {
			return navTab.name;
		}
	}
}


// Boolean variables
// 1. enlarged - Enlarges the video by resizing the two halves of the screen
// 2. aspect   - true = 21:9, false = 16:9, either mode
// 3. doWordList   - true = resize word list of tab as well, false = don't resize
// This enables the 'waterfall' effect when generating the list for the first time
// The current being viewed.  This is the one needing a resize
// 4. The tab to be resized: localTab, youtubeTab
function setLayout(enlarged, aspect, doWordList, tab) {

	// Below 1200 the screen expands vertically beyond the visible area to compensate for a smaller size.
	var documentHeight = (screen.width <= 1200) ? document.body.scrollHeight : window.innerHeight;

	// Redo tabs
	resizeNavTabContent();

	// If this code is triggered by a window resize event then this sets defaults
	// Can also be accessed by setting enlarged to resize
	if (enlarged.type == "resize" || enlarged == "resize") {

		doWordList = true;
		tab = getActiveNavTab();

		var infoDivOpacity;
		// Preserves the waterfall effect even if the screen is resized before first opening
		// Also keeps any enlarged/aspect ratio changes
		switch (tab) {
			case "youtubeTab":
				infoDivOpacity = window.getComputedStyle(document.getElementById("youtubeInfoDiv"), null).opacity;
				doWordList = (infoDivOpacity > 0 ? true : false);
				enlarged = youtubeEnlarged;
				if (enlarged == true) { document.getElementById("youtubeEnlargeVideo").value = "Shrink Video"; }
				aspect = youtubeAspectRatio;
				break;

			case "localTab":
				infoDivOpacity = window.getComputedStyle(document.getElementById("localVideoInfoDiv"), null).opacity;
				doWordList = (infoDivOpacity > 0 ? true : false);
				enlarged = localEnlarged
				if (enlarged == true) { document.getElementById("localVideoEnlargeVideo").value = "Shrink Video"; }
				aspect = localAspectRatio
				break;

			case "dictionaryTab":
				doWordList = (dictionaryUsed == true ? true : false);
				break;
		}
	}

	var videoContainer;
	var infoDiv;
	var container;
	var heading;
	var filter;
	var wordList;
	var rightHandSideContainer;
	var enlargedPortraitInfoDiv;

	if (tab == "localTab") {
		videoContainer = document.getElementById("localVideoContainer");
		infoDiv = document.getElementById("localVideoInfoDiv");
		container = document.getElementById("localVideoContent");
		heading = document.getElementById("localVideoFilterHeading");
		filter = document.getElementById("localVideoFilterField");
		wordList = document.getElementById("localVideoWordListContainer");
		rightHandSideContainer = document.getElementById("localVideoWordsAndFilter");
		enlargeButton = document.getElementById("localVideoEnlargeVideo");
		enlargedPortraitInfoDiv = localInfoDivEnlarged;

	} else if (tab == "youtubeTab") {
		videoContainer = document.getElementById("iframeContainer");
		infoDiv = document.getElementById("youtubeInfoDiv");
		container = document.getElementById("youtubeContent");
		heading = document.getElementById("youtubeFilterHeading");
		filter = document.getElementById("youtubeFilterField");
		wordList = document.getElementById("youtubeWordListContainer");
		rightHandSideContainer = document.getElementById("wordsAndFilter");
		enlargeButton = document.getElementById("youtubeEnlargeVideo");
		enlargedPortraitInfoDiv = youtubeInfoDivEnlarged;
	} else {
		videoContainer = document.getElementById("dictionaryTextArea");
		infoDiv = document.getElementById("dictionaryInfoDiv");
		container = document.getElementById("dictionaryDataEntry");
		heading = document.getElementById("dictionaryFilterHeading");
		filter = document.getElementById("dictionaryFilterField");
		wordList = document.getElementById("dictionaryWordListContainer");
		rightHandSideContainer = document.getElementById("dictionaryContent");
		enlargeButton = "null";
	}

	var height;
	var heightToBottom;

	if (window.matchMedia("(orientation: portrait)").matches) {

		if (heading.parentNode == rightHandSideContainer) { rightHandSideContainer.removeChild(heading); }
		if (heading.parentNode == rightHandSideContainer) { rightHandSideContainer.removeChild(filter); }
		if (heading.parentNode == rightHandSideContainer) { rightHandSideContainer.removeChild(wordList); }

		// Prevents some needless transitions if checked first
		if (heading.parentNode != container) { container.appendChild(heading); }
		if (filter.parentNode != container) { container.appendChild(filter); }
		if (wordList.parentNode != container) { container.appendChild(wordList); }

		rightHandSideContainer.style.width = "0%";
		container.style.width = "100%";
		videoContainer.style.width = "100%";

		if (tab == "dictionaryTab") {
			height = ((videoContainer.offsetWidth / 16) * 9).toFixed(2);
		} else {
			if (aspect == true) {
				height = ((videoContainer.offsetWidth / 21) * 9).toFixed(2); // 21:9
			} else {
				height = ((videoContainer.offsetWidth / 16) * 9).toFixed(2); // 16:9			
			}
		}
		
		if (videoContainer !== null) {
			videoContainer.style.height = height + "px";
		}


		// Video can no longer be enlarged, so the option to enlarge the text area is now present
		if (enlargeButton) {
			if (enlargedPortraitInfoDiv == true) {
				enlargeButton.value = "Shrink text area";
			} else {
				enlargeButton.value = "Enlarge text area";

				// Dictionary should be smaller as it's less used'
				(tab == "dictionaryTab") ? infoDiv.style.height = "6vh" : infoDiv.style.height = "12vh";

			}
		}	





		if (doWordList == true) {
			//heightToBottom = (window.innerHeight - wordList.offsetTop) - 30;
			//heightToBottom = (document.body.scrollHeight - wordList.offsetTop) - 30;
			heightToBottom = (documentHeight - wordList.offsetTop) - 30;

			wordList.style.paddingRight = "5px";
			wordList.style.paddingBottom = "5px";
			wordList.style.height = heightToBottom + "px";
		}
	}


	if (window.matchMedia("(orientation: landscape)").matches) {

		if (heading.parentNode == container) { container.removeChild(heading) }
		if (heading.parentNode == container) { container.removeChild(filter) }
		if (heading.parentNode == container) { container.removeChild(wordList) }

		// Prevents some needless transitions if checked first 		
		if (heading.parentNode != rightHandSideContainer) { rightHandSideContainer.appendChild(heading); }
		if (filter.parentNode != rightHandSideContainer) { rightHandSideContainer.appendChild(filter); }
		if (wordList.parentNode != rightHandSideContainer) { rightHandSideContainer.appendChild(wordList); }



		if (enlarged == true) {
			// Smaller screens can't enlarge as far'
			if (screen.width < 1950) {
				container.style.width = "55%";
				rightHandSideContainer.style.width = "44.5%";
			} else {
				container.style.width = "70%";
				rightHandSideContainer.style.width = "29.5%";
			}


		} else {
			container.style.width = "49.5%";
			rightHandSideContainer.style.width = "49.5%";
		}

		if (enlargeButton != "null") {
			if (enlargeButton.value == "Enlarge text area" || enlargeButton.value == "Shrink text area") {
				enlargeButton.value = "Enlarge video";
			}
		}

		if (doWordList == true) {
			//heightToBottom = (window.innerHeight - wordList.offsetTop) - 30;
			//heightToBottom = (document.body.scrollHeight - wordList.offsetTop) - 30;
			heightToBottom = (documentHeight - wordList.offsetTop) - 30;

			wordList.style.paddingRight = "5px";
			wordList.style.paddingBottom = "5px";
			wordList.style.height = heightToBottom + "px";
		}


		if (videoContainer !== null) {
			videoContainer.style.width = "100%";
			if (tab == "dictionaryTab") {
				// Dictionary textarea should be longer
				height = ((videoContainer.offsetWidth / 14) * 9).toFixed(2);
			} else {
				if (aspect == true) {
					height = ((videoContainer.offsetWidth / 21) * 9).toFixed(2); // 21:9
				} else {
					height = ((videoContainer.offsetWidth / 16) * 9).toFixed(2); // 16:9			
				}
			}

			videoContainer.style.height = height + "px";
			//heightToBottom = (window.innerHeight - (infoDiv.offsetTop)) - 30;
			//heightToBottom = (document.body.scrollHeight - infoDiv.offsetTop) - 30;
		}
		
		heightToBottom = (documentHeight - infoDiv.offsetTop) - 30;
		infoDiv.style.height = heightToBottom + "px";

	}// Landscape End
}


// Dynamically resize the tab as the window changes
window.onresize = setLayout;
window.onorientationchange = setLayout;

// Start on the settings tab
document.getElementById("settingsTabContent").style.display = "block";
document.getElementById("navTabContainer").children[3].focus();
resizeNavTabContent();


